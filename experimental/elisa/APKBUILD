# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=elisa
pkgver=20.04.3
pkgrel=0
pkgdesc="KDE music player"
url="https://kde.org/applications/multimedia/org.kde.elisa"
arch="all"
license="LGPL-3.0-only"
depends="qt5-qtquickcontrols2"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtmultimedia-dev vlc-dev
	qt5-qtquickcontrols2-dev qt5-qtsvg-dev cmake extra-cmake-modules
	baloo-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdeclarative-dev kdoctools-dev kfilemetadata-dev
	ki18n-dev kio-dev kirigami2-dev kpackage-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/elisa-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	# QML tests require OpenGL.
	# databaseInterfaceTest known failure: https://phabricator.kde.org/D24563#563126
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(elisaqmltests|databaseInterfaceTest)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="00afa92eaab736f5f38759a3076b9f579ee3f5fce84404042a73e11cdfe0e004b32a83f9e542ca6d0a962d66bcb3fcce9d12fc5dbdd8f937d9d4cea9321687b4  elisa-20.04.3.tar.xz"
