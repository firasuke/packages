This drops the use of the chromium sandbox syscall headers which were
defining syscall numbers if they were undefined. This masked the time64
issue initially since while musl renamed several of the time32 syscall
numbers to catch breakage like this, these headers were silently
bringing them back. I did this by comparing the syscall numbers provided
by the chromium and musl headers and redefining the generic names to
their time64 counterparts.

For gettimeofday and settimeofday there does not appear to be a time64
counterpart so I have defined them as the time32 versions. For
settimeofday this should not matter (the seccomp filter will block this
by virture of not being on the whitelist - no content process needs to
set the time anyway).

It is not possible to entirely block the usage of time32 syscalls
because musl uses them internally when it can or in fallback paths.

I did not check the MIPS headers since we don't currently ship a MIPS
port, so in the future those includes should be examined and dropped
too...

--- firefox-68.8.0/security/sandbox/chromium/sandbox/linux/system_headers/linux_syscalls.h	2020-04-29 16:49:45.000000000 -0500
+++ firefox-68.8.0/security/sandbox/chromium/sandbox/linux/system_headers/linux_syscalls.h	2020-05-20 03:09:47.369457646 -0500
@@ -8,18 +8,7 @@
 
 #ifndef SANDBOX_LINUX_SYSTEM_HEADERS_LINUX_SYSCALLS_H_
 #define SANDBOX_LINUX_SYSTEM_HEADERS_LINUX_SYSCALLS_H_
-
-#if defined(__x86_64__)
-#include "sandbox/linux/system_headers/x86_64_linux_syscalls.h"
-#endif
-
-#if defined(__i386__)
-#include "sandbox/linux/system_headers/x86_32_linux_syscalls.h"
-#endif
-
-#if defined(__arm__) && defined(__ARM_EABI__)
-#include "sandbox/linux/system_headers/arm_linux_syscalls.h"
-#endif
+#include <sys/syscall.h>
 
 #if defined(__mips__) && (_MIPS_SIM == _ABIO32)
 #include "sandbox/linux/system_headers/mips_linux_syscalls.h"
@@ -33,5 +22,36 @@
 #include "sandbox/linux/system_headers/arm64_linux_syscalls.h"
 #endif
 
+#if !defined(__NR_clock_getres) && defined(__NR_clock_getres_time64)
+#define __NR_clock_getres __NR_clock_getres_time64
+#endif
+#if !defined(__NR_clock_gettime) && defined(__NR_clock_gettime64)
+#define __NR_clock_gettime __NR_clock_gettime64
+#endif
+#if !defined(__NR_clock_nanosleep) && defined(__NR_clock_nanosleep_time64)
+#define __NR_clock_nanosleep __NR_clock_nanosleep_time64
+#endif
+#if !defined(__NR_clock_settime) && defined(__NR_clock_settime64)
+#define __NR_clock_settime __NR_clock_settime64
+#endif
+#if !defined(__NR_gettimeofday) && defined(__NR_gettimeofday_time32)
+#define __NR_gettimeofday __NR_gettimeofday_time32
+#endif
+#if !defined(__NR_settimeofday) && defined(__NR_settimeofday_time32)
+#define __NR_settimeofday __NR_settimeofday_time32
+#endif
+#if !defined(__NR_timer_gettime) && defined(__NR_timer_gettime64)
+#define __NR_timer_gettime __NR_timer_gettime64
+#endif
+#if !defined(__NR_timer_settime) && defined(__NR_timer_settime64)
+#define __NR_timer_settime __NR_timer_settime64
+#endif
+#if !defined(__NR_timerfd_gettime) && defined(__NR_timerfd_gettime64)
+#define __NR_timerfd_gettime __NR_timerfd_gettime64
+#endif
+#if !defined(__NR_timerfd_settime) && defined(__NR_timerfd_settime64)
+#define __NR_timerfd_settime __NR_timerfd_settime64
+#endif
+
 #endif  // SANDBOX_LINUX_SYSTEM_HEADERS_LINUX_SYSCALLS_H_
 
--- firefox-68.8.0/security/sandbox/linux/SandboxFilter.cpp	2020-04-29 16:49:45.000000000 -0500
+++ firefox-68.8.0/security/sandbox/linux/SandboxFilter.cpp	2020-05-19 23:33:27.829642593 -0500
@@ -478,6 +478,9 @@ class SandboxPolicyCommon : public Sandb
 
         // Thread synchronization
       case __NR_futex:
+#ifdef __NR_futex_time64
+      case __NR_futex_time64:
+#endif
         // FIXME: This could be more restrictive....
         return Allow();
 
@@ -488,6 +491,9 @@ class SandboxPolicyCommon : public Sandb
       case __NR_epoll_pwait:
       case __NR_epoll_ctl:
       case __NR_ppoll:
+#ifdef __NR_ppoll_time64
+      case __NR_ppoll_time64:
+#endif
       case __NR_poll:
         return Allow();
 
@@ -1017,6 +1023,9 @@ class ContentSandboxPolicy : public Sand
 
       CASES_FOR_select:
       case __NR_pselect6:
+#ifdef __NR_pselect6_time64
+      case __NR_pselect6_time64:
+#endif
         return Allow();
 
       CASES_FOR_getdents:
