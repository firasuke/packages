# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpmcore
pkgver=4.1.0
pkgrel=0
pkgdesc="Core routines for KDE Partition Manager"
url="https://www.kde.org/applications/system/partitionmanager"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="libatasmart-dev parted-dev qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules kcoreaddons-dev ki18n-dev
	kwidgetsaddons-dev util-linux-dev kauth-dev qca-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/kpmcore/$pkgver/src/kpmcore-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c45b31a41c62a68aee991be0f7954f4bd2136660578cc60f7fa6f05e56716458421e16cd314b3b9a14b56fdc9377db511c1645e5c41f0446d86a8ac5bb66c273  kpmcore-4.1.0.tar.xz"
