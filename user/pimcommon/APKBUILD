# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pimcommon
pkgver=20.08.1
pkgrel=0
pkgdesc="Common files for KDE PIM software"
url="https://kontact.kde.org/"
arch="all"
license="LGPL-2.0+ AND GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev qt5-qttools-dev cmake extra-cmake-modules boost-dev
	karchive-dev kauth-dev kbookmarks-dev kcmutils-dev kcodecs-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcontacts-dev
	kcoreaddons-dev ki18n-dev kimap-dev kio-dev kitemmodels-dev
	kitemviews-dev kjobwidgets-dev kldap-dev kmime-dev knewstuff-dev
	kpimtextedit-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev libkdepim-dev purpose-dev solid-dev
	akonadi-dev akonadi-contacts-dev akonadi-mime-dev akonadi-search-dev
	attica-dev kcalendarcore-dev libxslt-dev sonnet-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/pimcommon-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="418c6d10bf789a4554c818d4a476b01e1a7bd21402ee0f4c9fbadced29b853ac9ef5d619ba7a448e4f7adde725e4b72d8c5ed5ed740dd654467e2d06f6e0e28d  pimcommon-20.08.1.tar.xz
d231227e1188fdb976e223d0b3c2fe30ddb2a555b0498a4e49deeb519e44a75c5ab6afee0d68fd53c407e1b6bcebcfb8fa33378b92420952baef082ed4a55c39  lts.patch"
