# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=freetype
pkgver=2.10.4
pkgrel=0
pkgdesc="TrueType font rendering library"
url="https://www.freetype.org/"
arch="all"
license="FTL OR GPL-2.0+"
options="!check"
depends=""
makedepends="zlib-dev libpng-dev bzip2-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.savannah.gnu.org/releases/freetype/freetype-$pkgver.tar.xz"

# secfixes:
#   2.10.4-r0:
#     - CVE-2020-15999
#   2.9.1-r0:
#     - CVE-2018-6942
#   2.7.1-r1:
#     - CVE-2017-8105
#     - CVE-2017-8287

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static \
		--with-bzip2 \
		--with-png \
		--enable-freetype-config
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="827cda734aa6b537a8bcb247549b72bc1e082a5b32ab8d3cccb7cc26d5f6ee087c19ce34544fa388a1eb4ecaf97600dbabc3e10e950f2ba692617fee7081518f  freetype-2.10.4.tar.xz"
