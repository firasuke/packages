# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=spectacle
pkgver=20.08.1
pkgrel=0
pkgdesc="Application for capturing desktop screenshots"
url="https://www.kde.org/applications/graphics/spectacle/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kcoreaddons-dev kwidgetsaddons-dev kdbusaddons-dev knotifications-dev
	kconfig-dev ki18n-dev kio-dev kxmlgui-dev kwindowsystem-dev python3
	kdoctools-dev kdeclarative-dev xcb-util-image-dev xcb-util-cursor-dev
	libxcb-dev xcb-util-renderutil-dev knewstuff-dev libkipi-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/spectacle-$pkgver.tar.xz
	no-wayland.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="83adc382c66173c3733dc603a7e7b957856b735af7764c66624158f39ba8bc8ce08e12d46d12781d5a21c2e09ba17ebb651f62dcf06ad032c409aae68f1b42a0  spectacle-20.08.1.tar.xz
a138cc146e998648d2d99b21c935ab648751c6d66d7be6030e25829a3c001384811066099e3da3f0d4c546c62fbcac243a6a2c54aac8367bcc07bd89c683f7df  no-wayland.patch"
