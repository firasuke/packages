# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=quaternion
pkgver=0.0.9.4e
_pkgver=$(printf '%s' "$pkgver" | sed 's/_/-/')
pkgrel=0
pkgdesc="Qt5-based Matrix chat client"
url="https://matrix.org/docs/projects/client/quaternion.html"
arch="all"
license="GPL-3.0+"
depends="qt5-qtquickcontrols"
makedepends="cmake libquotient-dev qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev qt5-qttools-dev"
subpackages=""
source="quaternion-$pkgver.tar.gz::https://github.com/QMatrixClient/Quaternion/archive/$_pkgver.tar.gz"
builddir="$srcdir/Quaternion-$_pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5478892b7dce96a60f1429ee6dff9dd0b4fbb89bf45ecc2e499ee90f615af4de12d1de0002804420a85a21a6ba533e4799c94d2411067cb7d7aacca183d9d981  quaternion-0.0.9.4e.tar.gz"
