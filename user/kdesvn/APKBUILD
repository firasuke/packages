# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesvn
pkgver=2.1.0_p1
dlver=$(echo $pkgver | sed s/_p/-/)
pkgrel=0
pkgdesc="Graphical Subversion version control client"
url="https://kde.org/applications/development/org.kde.kdesvn"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kbookmarks-dev kcodecs-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kauth-dev
	kdbusaddons-dev ki18n-dev kiconthemes-dev kitemviews-dev kjobwidgets-dev
	kio-dev knotifications-dev kparts-dev kservice-dev ktextwidgets-dev
	kwallet-dev kwidgetsaddons-dev kxmlgui-dev apr-dev subversion-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kdesvn/${pkgver%_*}/kdesvn-$dlver.tar.xz"
builddir="$srcdir"/kdesvn-${pkgver%_*}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7d651abb99420caa368dec8d58bf80fc7cf75113f4f3639035835b2cef250de9e0cffab46f3d8efbdd33220b44cea77aa42f2f2ac54874f1ea6301b4718516ca  kdesvn-2.1.0-1.tar.xz"
