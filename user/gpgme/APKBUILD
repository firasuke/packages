# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gpgme
pkgver=1.13.1
pkgrel=0
pkgdesc="GnuPG Made Easy"
url="https://www.gnupg.org/related_software/gpgme/"
arch="all"
# gpgme-tool: GPL3; lib is mixture of the rest
license="(LGPL-3.0+ OR GPL-2.0+) AND LGPL-2.1+ AND MIT AND GPL-3.0+"
depends="gnupg"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev doxygen libassuan-dev libgpg-error-dev python3-dev
	swig cmd:which"
subpackages="$pkgname-dev $pkgname-doc gpgmepp qgpgme py3-gpg:_py"
source="https://gnupg.org/ftp/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-languages=cl,cpp,python,qt
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

qgpgme() {
	pkgdesc="$pkgdesc (Qt 5 library)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libqgpgme.so* "$subpkgdir"/usr/lib/
}

gpgmepp() {
	pkgdesc="C++ bindings for GPGME"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgpgmepp.so.* "$subpkgdir"/usr/lib/
}

_py() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="$pkgname=$pkgver-r$pkgrel python3"

	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/python* "$subpkgdir"/usr/lib/
}

sha512sums="11de670c6cf512508103fe67af56d9fbb2a9dda6fc6fa3cd321371bbe337c7c2c81913ca557d07187adb2a63d37ea1a44da97ab22345bbe6022c405d0cb083b8  gpgme-1.13.1.tar.bz2"
