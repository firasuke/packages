# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=drkonqi
pkgver=5.18.5
pkgrel=0
pkgdesc="Crash diagnostic system for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11
license="GPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND BSD-2-Clause"
depends=""
makedepends="cmake extra-cmake-modules kauth-dev kcodecs-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev kidletime-dev
	kio-dev kitemviews-dev kjobwidgets-dev knotifications-dev kservice-dev
	kwallet-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev
	qt5-qtbase-dev qt5-qtx11extras-dev solid-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/drkonqi-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b4862297e969f501f526880560acbd02026b64a0bec2f6e3b2b30f4bfe5ff2abd1798e3bda920bd61adbb6c9a3455e0c4f304f3f37bcafed3f3d8e6de1bc67a9  drkonqi-5.18.5.tar.xz"
