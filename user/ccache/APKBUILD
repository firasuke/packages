# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ccache
pkgver=3.7.11
pkgrel=0
pkgdesc="A fast C/C++ compiler cache"
url="https://ccache.dev/"
arch="all"
license="GPL-3.0+"
depends=""
checkdepends="bash perl"
makedepends="zlib-dev"
subpackages="$pkgname-doc"
source="https://github.com/ccache/ccache/releases/download/v$pkgver/$pkgname-$pkgver.tar.xz"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make test
}

package() {
	install -Dm 755 ccache "$pkgdir"/usr/bin/ccache
	install -Dm 644 doc/ccache.1 "$pkgdir"/usr/share/man/man1/ccache.1

	local link=
	mkdir -p "$pkgdir"/usr/lib/ccache/bin

	for link in cc gcc g++ cpp c++ ${CHOST}-cc ${CHOST}-gcc \
			${CHOST}-g++ ${CHOST}-cpp ${CHOST}-c++; do
		ln -sf /usr/bin/ccache "$pkgdir"/usr/lib/ccache/bin/$link
	done
}

sha512sums="47b71b3ab4b89bf9b6f6d15941c22d2207b369922f51f9a8fbf4e86554325053dccce669e1c1ed3c0019a7e0a3d610f399e52d3fee68b56111aa7c1b24132c11  ccache-3.7.11.tar.xz"
