# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=systemsettings
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE system settings configuration utility"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+"
depends="kirigami2"
depends_dev="qt5-qtbase-dev kitemviews-dev kservice-dev kwidgetsaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules kactivities-stats-dev
	kactivities-dev kauth-dev kcmutils-dev kcompletion-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev khtml-dev ki18n-dev
	kiconthemes-dev kitemmodels-dev kio-dev kirigami2-dev kwindowsystem-dev
	kxmlgui-dev libkworkspace-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/systemsettings-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b6b9184570f188878daca140bd81ee13d47ba867eac5289b67295de9918205e5ebc7f7d644968fbbd9d9dfe1395cae6bd29f7ba4c4aa9a82f85b912693ae4e6a  systemsettings-5.18.5.tar.xz"
