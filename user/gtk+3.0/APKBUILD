# Maintainer: 
pkgname=gtk+3.0
pkgver=3.24.24
pkgrel=0
pkgdesc="The GTK+ Toolkit (v3)"
url="https://www.gtk.org/"
arch="all"
options="!check"  # Test suite is known to fail upstream
license="LGPL-2.1+"
depends="adwaita-icon-theme shared-mime-info gtk-update-icon-cache"
makedepends="at-spi2-atk-dev atk-dev cairo-dev cups-dev expat-dev
	fontconfig-dev gdk-pixbuf-dev glib-dev gnutls-dev
	gobject-introspection-dev libepoxy-dev libice-dev libx11-dev
	libxcomposite-dev libxcursor-dev libxdamage-dev libxext-dev
	libxfixes-dev libxi-dev libxinerama-dev libxrandr-dev pango-dev
	tiff-dev wayland-dev wayland-protocols libxkbcommon-dev zlib-dev"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.post-deinstall"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/gtk+/${pkgver%.*}/gtk+-$pkgver.tar.xz"
builddir="$srcdir"/gtk+-$pkgver

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-wayland-backend \
		--enable-xkb \
		--enable-xinerama \
		--enable-xrandr \
		--enable-xfixes \
		--enable-xcomposite \
		--enable-xdamage \
		--enable-x11-backend

	# https://bugzilla.gnome.org/show_bug.cgi?id=655517
	sed -i 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

	make
}

package() {
	make DESTDIR="$pkgdir" install

	# use gtk-update-icon-cache from gtk+2.0 for now
	rm -f "$pkgdir"/usr/bin/gtk-update-icon-cache
	rm -f "$pkgdir"/usr/share/man/man1/gtk-update-icon-cache.1
}

sha512sums="f4e22d7c78d7c641ac7c3beeb3181000b72b025c65074bab6bd51b9a86ec4f6a2e25f06ad7671a908be421c064f4c26e2185febb424f310a2530561aebde8520  gtk+-3.24.24.tar.xz"
