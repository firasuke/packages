# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=icu
pkgver=65.1

# convert x.y.z to x_y_z
_ver=$(printf '%s' "$pkgver" | tr . _)

pkgrel=1
pkgdesc="International Components for Unicode"
url="http://site.icu-project.org/"
arch="all"
license="ICU"
depends=""
checkdepends="diffutils python3"
makedepends=""
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-libs"
source="https://github.com/unicode-org/icu/releases/download/release-$(printf '%s' "$pkgver" | tr . -)/${pkgname}4c-$_ver-src.tgz
	icu-60.2-always-use-utf8.patch
	CVE-2020-10531.patch
	"

# secfixes:
#   57.1-r1:
#     - CVE-2016-6293
#   58.1-r1:
#     - CVE-2016-7415
#   58.2-r2:
#     - CVE-2017-7867
#     - CVE-2017-7868
#   65.1-r1:
#     - CVE-2020-10531

builddir="$srcdir"/icu/source

prepare() {
	default_prepare
	update_config_sub

	# https://bugs.icu-project.org/trac/ticket/6102
	for x in ARFLAGS CFLAGS CPPFLAGS CXXFLAGS FFLAGS LDFLAGS; do
		sed -i -e "/^${x} =.*/s:@${x}@::" "config/Makefile.inc.in"
	done
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-shared \
		--enable-static \
		--with-data-packaging=library \
		--disable-samples \
		--mandir=/usr/share/man
	# Weird build system glitch.
	mkdir data/out
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	chmod +x "$pkgdir"/usr/bin/icu-config
	install -Dm644 "$srcdir"/icu/license.html \
		"$pkgdir"/usr/share/licenses/icu/license.html
}

static() {
	pkgdesc="$pkgdesc (Static libraries)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

sha512sums="8f1ef33e1f4abc9a8ee870331c59f01b473d6da1251a19ce403f822f3e3871096f0791855d39c8f20c612fc49cda2c62c06864aa32ddab2dbd186d2b21ce9139  icu4c-65_1-src.tgz
f86c62422f38f6485c58d4766e629bab69e4b0e00fa910854e40e7db1ace299152eaefa99ae2fbab7465e65d3156cbea7124612defa60680db58ab5c34d6262f  icu-60.2-always-use-utf8.patch
2901235a24b1973516e495473188b077eb0486714ac47f702f226a3406176e913672159978b67548a1316284ecc716e06158b05c190ddfa83ad576a7a3539d72  CVE-2020-10531.patch"
